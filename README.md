Koa-mongoose-erd-generator
===============================

A tool that generates an ERD-diagram from Mongoose-models definitions. The diagram can be included as a Koa-route by using a template html-file. 

Forked from: https://github.com/jodevsa/mongoose-erd-generator/commits?author=jodevsa. The original author is Subhi Al Hasan.


## Example
```js
const Koa = require("koa");
const dbSchema = require("koa-mongoose-erd-generator");

// create server
const server = new Koa();

// Serve documentation
// This hosts the database ERD diagram at the "/docs" location.
// modelsPath defines the folder of your mongoose models. 
// nameColor defines the color theme of the diagram.
// TEMPLATE_HTML_FILE is a html file in which the diagramm will be embedded. 
// The diagram will replace the String "fakeDbSchema" in the html document.
server.use(
  dbSchema(
    "/docs",
    { modelsPath:"PATH_TO_MONGOOSE_MODELS", nameColor: "#007bff" },
    "TEMPLATE_HTML_FILE"
  )
);

server.listen(80);
```
